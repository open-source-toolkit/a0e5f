# Inno Setup 6.2.1 及中文语言包资源下载

## 简介

本仓库提供 Inno Setup 6.2.1 安装包工具及其中文语言包的下载资源。Inno Setup 是一款功能强大且易于使用的 Windows 安装包制作工具，支持 Pascal 脚本，能够在几分钟内生成标准的 Windows 安装包。

## 资源内容

- **Inno Setup 6.2.1**: 最新版本的 Inno Setup 安装包工具，支持多种高级功能，如自定义安装界面、脚本编写等。
- **中文语言包**: 为 Inno Setup 提供中文界面支持，方便中文用户使用。
- **加密 DLL**: 包含用于加密和保护安装包的 DLL 文件。

## 使用说明

1. **下载资源**: 从本仓库下载 Inno Setup 6.2.1 安装包及中文语言包。
2. **安装 Inno Setup**: 运行 Inno Setup 6.2.1 安装包，按照提示完成安装。
3. **安装中文语言包**: 将下载的中文语言包文件放置在 Inno Setup 的安装目录中，并配置 Inno Setup 使用中文界面。
4. **开始制作安装包**: 使用 Inno Setup 创建和生成 Windows 安装包。

## 注意事项

- 请确保下载的资源文件来自可信的来源，以避免潜在的安全风险。
- 在使用加密 DLL 时，请遵守相关法律法规，确保合法使用。

## 贡献与反馈

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交 Issue 或 Pull Request。我们非常乐意与您一起改进和完善这个资源。

## 许可证

本仓库提供的资源遵循 Inno Setup 的原始许可证。请在使用前仔细阅读相关许可证条款。

---

感谢您使用本仓库提供的资源，祝您在使用 Inno Setup 时顺利愉快！